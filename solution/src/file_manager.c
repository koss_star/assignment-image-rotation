#include "file_manager.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

static enum file_open_status parse_open_file_result(int err);

enum file_open_status file_open_to_read_bytes(const char * file_name, FILE ** f) {
    if (file_name == NULL || strcmp(file_name, "") == 0 || f == NULL) {
        return FILE_OPEN_STATUS_EMPTY_FILENAME;
    }
    *f = fopen(file_name, "rb");
    if (*f == NULL) {
        return parse_open_file_result(errno);
    }
    return FILE_OPEN_STATUS_OK;
}

enum file_open_status file_open_to_write_bytes(const char * file_name, FILE ** f) {
    if (file_name == NULL || strcmp(file_name, "") == 0 || f == NULL) {
        return FILE_OPEN_STATUS_EMPTY_FILENAME;
    }
    *f = fopen(file_name, "wb");
    if (*f == NULL) {
        return parse_open_file_result(errno);
    }
    return FILE_OPEN_STATUS_OK;
}

const char * file_get_open_err_msg(const enum file_open_status status) {
    switch (status) {
        case FILE_OPEN_STATUS_OK:
            return "";
        case FILE_OPEN_STATUS_EMPTY_FILENAME:
            return "Expected not empty file name";
        case FILE_OPEN_STATUS_NOT_FOUND:
            return "File not found";
        case FILE_OPEN_ILLEGAL_ACCESS:
            return "Illegal access";
        case FILE_OPEN_UNKNOWN_ERROR:
        default:
            return "Unknown error";
    }
}

void file_close(FILE * f) {
    fclose(f);
}

static enum file_open_status parse_open_file_result(const int err) {
    switch (err) {
        case 0:
            return FILE_OPEN_STATUS_OK;
        case ENOENT:
            return FILE_OPEN_STATUS_NOT_FOUND;
        case EACCES:
            return FILE_OPEN_ILLEGAL_ACCESS;
        default:
            return FILE_OPEN_UNKNOWN_ERROR;
    }
}
