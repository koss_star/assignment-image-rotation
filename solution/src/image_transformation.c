#include "image_transformation.h"

#include <math.h>
#include <stdint.h>

#include "image.h"

struct coordinates {
    uint64_t x;
    uint64_t y;
};

static struct coordinates rotate_90(const struct coordinates source);
static struct coordinates transfer(struct coordinates source, int64_t dx, int64_t dy);

struct image image_rotate_90(const struct image * source_img) {
    struct image rotated_img = image_create(source_img->height, source_img->width);
    for (uint64_t y = 0; y < source_img->height; y++) {
        for (uint64_t x = 0; x < source_img->width; x++) {
            const struct coordinates source = (struct coordinates) { .x = x, .y = y };
            const struct coordinates rotated = rotate_90(source);
            const struct coordinates dest = transfer(rotated, (int64_t) (source_img->height - 1), 0);
            image_set_pixel(&rotated_img, dest.x, dest.y, image_get_pixel(source_img, x, y));
        }
    }
    return rotated_img;
}

static struct coordinates rotate_90(const struct coordinates source) {
    return (struct coordinates) {
        .x = -source.y,
        .y = source.x
    };
}

static struct coordinates transfer(const struct coordinates source, const int64_t dx, const int64_t dy) {
    return (struct coordinates) {
        .x = source.x + dx,
        .y = source.y + dy
    };
}
