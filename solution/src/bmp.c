#include "bmp.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "image.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool bmp_header_read(FILE * in, struct bmp_header * header);
static bool bmp_read_image_padding(FILE * in, const struct image * img);
static bool bmp_read_image_row(FILE * in, struct pixel * row, size_t size);
static bool bmp_read_image(FILE * in, struct image * img);
static enum bmp_read_status bmp_header_check(const struct bmp_header * header);
static uint8_t bmp_image_calc_padding(const struct image * img);
static bool bmp_header_write(FILE * out, const struct bmp_header * header);
static bool bmp_write_image_padding(FILE * out, const struct image * img);
static bool bmp_write_image_row(FILE * out, const struct pixel * row, size_t size);
static bool bmp_write_image(FILE * out, const struct image * img);
static struct bmp_header bmp_header_from_image(const struct image * img);

enum bmp_read_status bmp_read(FILE * in, struct image * img) {
    if (in == NULL)
        return BMP_READ_NULL_FILE;

    struct bmp_header header = { 0 };
    if (!bmp_header_read(in, &header))
        return BMP_READ_INVALID_HEADER;

    const enum bmp_read_status status = bmp_header_check(&header);
    if (status != BMP_READ_OK)
        return status;

    struct image read_img = image_create(header.biWidth, header.biHeight);
    if (!bmp_read_image(in, &read_img)) {
        image_destroy(&read_img);
        return BMP_READ_INVALID_IMAGE;
    }

    *img = read_img;
    return BMP_READ_OK;
}

const char * bmp_get_read_err_msg(const enum bmp_read_status status) {
    switch (status) {
        case BMP_READ_OK:
            return "";
        case BMP_READ_INVALID_SIGNATURE:
            return "Invalid file format";
        case BMP_READ_INVALID_HEADER:
            return "Invalid image header";
        case BMP_READ_INVALID_BIT_COUNT:
            return "Invalid color depth";
        case BMP_READ_INVALID_IMAGE:
            return "Invalid image";
        case BMP_READ_NULL_FILE:
            return "Expected not null file";
        default:
            return "Unknown error";
    }
}

const char * bmp_get_write_err_msg(const enum bmp_write_status status) {
    switch (status) {
        case BMP_WRITE_OK:
            return "";
        case BMP_WRITE_NULL_FILE:
            return "Expected not null file";
        case BMP_WRITE_ERROR:
        default:
            return "Unknown error";
    }
}

enum bmp_write_status bmp_write(FILE * const out, const struct image * img) {
    if (out == NULL)
        return BMP_WRITE_NULL_FILE;

    const struct bmp_header header = bmp_header_from_image(img);
    if (!bmp_header_write(out, &header))
        return BMP_WRITE_ERROR;

    if (!bmp_write_image(out, img))
        return BMP_WRITE_ERROR;
    return BMP_WRITE_OK;
}

static uint8_t bmp_image_calc_padding(const struct image * img) {
    const size_t byte_size = img->width * sizeof(struct pixel);
    return (4 - byte_size % 4) % 4;
}

static struct bmp_header bmp_header_from_image(const struct image * img) {
    struct bmp_header header = { 0 };
    header.bfType = 0x4D42;

    const uint8_t padding = bmp_image_calc_padding(img);
    header.biSizeImage = img->height * (sizeof(struct pixel) * img->width + (size_t) padding);

    header.bfileSize = sizeof(struct bmp_header) + header.biSizeImage;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = sizeof(struct pixel) * 8;
    return header;
}

static bool bmp_header_read(FILE * in, struct bmp_header * header) {
    return fread(header, sizeof(struct bmp_header), 1, in);
}

static bool bmp_header_write(FILE * out, const struct bmp_header * header) {
    return fwrite(header, sizeof(struct bmp_header), 1, out);
}

static bool bmp_read_image_padding(FILE * in, const struct image * img) {
    const uint8_t padding_size = bmp_image_calc_padding(img);
    uint8_t padding[padding_size];
    for (uint8_t i = 0; i < padding_size; i++)
        padding[i] = 0;
    return fread(padding, 1, padding_size, in);
}

static bool bmp_write_image_padding(FILE * out, const struct image * img) {
    const uint8_t padding_size = bmp_image_calc_padding(img);
    uint8_t padding[padding_size];
    for (uint8_t i = 0; i < padding_size; i++)
        padding[i] = 0;
    return fwrite(padding, 1, padding_size, out);
}

static bool bmp_read_image_row(FILE * in, struct pixel * row, const size_t size) {
    return fread(row, sizeof(struct pixel), size, in);
}

static bool bmp_write_image_row(FILE * out, const struct pixel * row, const size_t size) {
    return fwrite(row, sizeof(struct pixel), size, out);
}

static bool bmp_read_image(FILE * in, struct image * img) {
    for (uint64_t i = 0; i < img->height; i++) {
        struct pixel * row_i = image_get_pixel_row(img, i);
        if (!bmp_read_image_row(in, row_i, img->width) || !bmp_read_image_padding(in, img)) {
            return false;
        }
    }
    return true;
}

static bool bmp_write_image(FILE * out, const struct image * img) {
    for (uint64_t i = 0; i < img->height; i++) {
        const struct pixel * row_i = image_get_pixel_row(img, i);
        if (!bmp_write_image_row(out, row_i, img->width) || !bmp_write_image_padding(out, img)) {
            return false;
        }
    }
    return true;
}

static enum bmp_read_status bmp_header_check(const struct bmp_header * header) {
    if (header->bfType != 0x4D42 && header->bfType != 0x424D) {
        return BMP_READ_INVALID_SIGNATURE;
    }
    if (header->bfReserved != 0 || header->bOffBits < sizeof(struct bmp_header) || header->biPlanes != 1) {
        return BMP_READ_INVALID_HEADER;
    }
    if (header->biBitCount != 24) {
        return BMP_READ_INVALID_BIT_COUNT;
    }
    return BMP_READ_OK;
}
