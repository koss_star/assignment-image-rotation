#include "image.h"

#include <stdlib.h>

struct image image_create(const uint64_t width, const uint64_t height) {
    struct image img = {.height = height, .width = width, .data = NULL};
    img.data = malloc(sizeof(struct pixel) * width * height);
    return img;
}

void image_destroy(struct image * img) {
    free(img->data);
    img->data = NULL;
}

void image_set_pixel(struct image * img, const uint64_t x, const uint64_t y, const struct pixel value) {
    img->data[y * img->width + x] = value;
}

struct pixel image_get_pixel(const struct image * img, const uint64_t x, const uint64_t y) {
    return img->data[y * img->width + x];
}

struct pixel * image_get_pixel_row(const struct image * img, const uint64_t row) {
    return img->data + row * img->width;
}
