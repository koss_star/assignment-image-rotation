#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "file_manager.h"
#include "image.h"
#include "image_transformation.h"

static struct image read_image_from_file(const char * file_name);
static void write_image_to_file(const char * file_name, const struct image * img);

int main(int argc, char ** argv) {
    if (argc != 3)
        return -1;
    const char* input_file_name = argv[1];
    const char* output_file_name = argv[2];

    struct image img = read_image_from_file(input_file_name);
    struct image rotated_img = image_rotate_90(&img);
    write_image_to_file(output_file_name, &rotated_img);

    image_destroy(&img);
    image_destroy(&rotated_img);
    return 0;
}

static void assert_file_open_not_error(const char * file_name, const enum file_open_status status) {
    if (status != FILE_OPEN_STATUS_OK) {
        fprintf(stderr, "Can not open file %s. %s", file_name, file_get_open_err_msg(status));
        exit(1);
    }
}

static void assert_bmp_read_not_error(const char * file_name, const enum bmp_read_status status) {
    if (status != BMP_READ_OK) {
        fprintf(stderr, "Can not read bmp image %s. %s", file_name, bmp_get_read_err_msg(status));
        exit(1);
    }
}

static void assert_bmp_write_not_error(const char * file_name, const enum bmp_write_status status) {
    if (status != BMP_WRITE_OK) {
        fprintf(stderr, "Can not write bmp image %s. %s", file_name, bmp_get_write_err_msg(status));
        exit(1);
    }
}

static struct image read_image_from_file(const char * file_name) {
    FILE * f = NULL;
    const enum file_open_status file_open_status = file_open_to_read_bytes(file_name, &f);
    assert_file_open_not_error(file_name, file_open_status);
    struct image img = { 0 };
    const enum bmp_read_status bmp_read_status = bmp_read(f, &img);
    assert_bmp_read_not_error(file_name, bmp_read_status);
    file_close(f);
    return img;
}

static void write_image_to_file(const char * file_name, const struct image * img) {
    FILE * f = NULL;
    const enum file_open_status file_open_status = file_open_to_write_bytes(file_name, &f);
    assert_file_open_not_error(file_name, file_open_status);
    const enum bmp_write_status bmp_write_status = bmp_write(f, img);
    assert_bmp_write_not_error(file_name, bmp_write_status);
    file_close(f);
}
