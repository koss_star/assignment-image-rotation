#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel * data;
};

struct image image_create(uint64_t width, uint64_t height);
void image_destroy(struct image * img);
void image_set_pixel(struct image * img, uint64_t x, uint64_t y, struct pixel value);
struct pixel image_get_pixel(const struct image * img, uint64_t x, uint64_t y);
struct pixel * image_get_pixel_row(const struct image * img, uint64_t row);

#endif //IMAGE_ROTATION_IMAGE_H
