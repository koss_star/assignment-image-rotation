#ifndef IMAGE_ROTATION_BMP_H
#define IMAGE_ROTATION_BMP_H

#include <stdio.h>

#include "image.h"

enum bmp_read_status  {
    BMP_READ_OK = 0,
    BMP_READ_INVALID_SIGNATURE,
    BMP_READ_INVALID_BIT_COUNT,
    BMP_READ_INVALID_IMAGE,
    BMP_READ_INVALID_HEADER,
    BMP_READ_NULL_FILE
};

enum bmp_write_status  {
    BMP_WRITE_OK = 0,
    BMP_WRITE_NULL_FILE,
    BMP_WRITE_ERROR
};

enum bmp_read_status bmp_read(FILE * in, struct image * img);
const char * bmp_get_read_err_msg(enum bmp_read_status);

enum bmp_write_status bmp_write(FILE * out, const struct image * img);
const char * bmp_get_write_err_msg(enum bmp_write_status);

#endif //IMAGE_ROTATION_BMP_H
