#ifndef IMAGE_ROTATION_FILE_MANAGER_H
#define IMAGE_ROTATION_FILE_MANAGER_H

#include <stdio.h>

enum file_open_status {
    FILE_OPEN_STATUS_OK = 0,
    FILE_OPEN_STATUS_EMPTY_FILENAME,
    FILE_OPEN_STATUS_NOT_FOUND,
    FILE_OPEN_ILLEGAL_ACCESS,
    FILE_OPEN_UNKNOWN_ERROR,
};

enum file_open_status file_open_to_read_bytes(const char file_name[], FILE ** f);
enum file_open_status file_open_to_write_bytes(const char file_name[], FILE ** f);
const char * file_get_open_err_msg(enum file_open_status status);
void file_close(FILE * f);

#endif //IMAGE_ROTATION_FILE_MANAGER_H
