#ifndef IMAGE_ROTATION_IMAGE_TRANSFORMATION_H
#define IMAGE_ROTATION_IMAGE_TRANSFORMATION_H

#include "image.h"

struct image image_rotate_90(const struct image * source_img);

#endif //IMAGE_ROTATION_IMAGE_TRANSFORMATION_H
